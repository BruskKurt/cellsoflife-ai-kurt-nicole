package com.helmo.cellsoflife.models;

import java.util.Random;

public class Organisme implements Cloneable{

	private boolean[][] organisme;
	private Pattern pattern;

	
	/**
	 * Constructeur permetant la cr�ation d'un Organisme
	 * @param n nombre de lignes
	 * @param m nombre de colonnes
	 * @param loadedFromDatabase true si la partie est charg�e depuis la base de donn�e, false si c'est une g�n�ration basique.
	 */
	public Organisme(int n, int m, boolean loadedFromDatabase) {
		pattern = new Pattern();
		if (n < 0 || m < 0) {
			throw new IllegalArgumentException("L'organisme doit avoir des lignes/colonnes sup�rieurs � 0!");
		}

		this.organisme = new boolean[n][m];	
		
		for (int i = 0; i < organisme.length; i++) {
			for (int j = 0; j < organisme[0].length; j++) {
				organisme[i][j] = false;
			}
		}
	
		if(!loadedFromDatabase){
		organisme = genererOrganisme();
		}
	
	}
	
	/**
	 * Constructeur permettant la Deep Copy d'un organisme.
	 * @param org organisme � copier, "cloner" de mani�re ind�pendante.
	 */
	public Organisme(Organisme org){
		this.organisme = new boolean[org.getNumberLine()][org.getNumberColumn()];
		for (int i = 0; i < organisme.length; i++) {
			for (int j = 0; j < organisme[0].length; j++) {
				this.organisme[i][j] = org.getCell(i, j);
			}
		}
	}
	


	/**
	 * Retourne le statut d'une cellule de l'organisme
	 * 
	 * @param n ligne de la cellule
	 * @param m colonne de la cellule
	 * @return �tat de la cellule (true si infect�, false si saine)
	 */
	public boolean getCell(int n, int m) {
		if (n > organisme.length || m > organisme[0].length) {
			throw new IllegalArgumentException("La cellule n'existe pas !");
		}
		return organisme[n][m];
	}

	/**
	 * Retourne le nombre de lines de l'organisme
	 * 
	 * @return nombre ligne de l'organisme
	 */
	public int getNumberLine() {
		return organisme.length;
	}

	/**
	 * Retourne le nombre de colonnes de l'organisme
	 * 
	 * @return nombre de colonnes de l'organisme
	 */
	public int getNumberColumn() {
		return organisme[0].length;
	}

	/**
	 * Retourne un pourcentage de cellules infect�s
	 * 
	 * @return un double repr�sentant un pourcentage de taux d'infections (2 chiffres apr�s virgule).
	 */
	public double getPercentAffected() {
		double result = 0;
		double cellulesInfectes = 0;

		for (int i = 0; i < organisme.length; i++) {
			for (int j = 0; j < organisme[0].length; j++) {
				if (organisme[i][j] == true) {
					cellulesInfectes += 1;
				}
			}
		}
		result = (cellulesInfectes / (this.organisme.length * this.organisme[0].length)) * 100;
		result = Math.floor(result * 100) / 100;
		return result;
	}

	/**
	 * Affiche dans la console l'�tat de l'organisme (�quivalent � un toString()) 
	 */
	public void getOrganisme() {
		for (int i = 0; i < organisme.length; i++) {
			for (int j = 0; j < organisme[0].length; j++) {
				System.out.println("Valeur de organisme[" + i + "][" + j + "]  = " + organisme[i][j]);
			}
		}
	}

	/**
	 * Permet de choisir la valeur d'une case sp�cifique
	 * @param i ligne de la cellule
	 * @param j colonne de la cellule
	 * @param state �tat � affecter (true: infect�e, false: saine)
	 */
	public void setCells(int i, int j, boolean state) {
		if (i < organisme.length && j < organisme[0].length)
			this.organisme[i][j] = state;
	}
	
	
	/**
	 * Permet de calculer le nombre de cases voisines contamin�es
	 * @param col colonne de la cellule
	 * @param ligne ligne de la cellule
	 * @return nombre de cellules voisines infect�es
	 */
	public int calculeNbVoisinsContamines(int col, int ligne) {
		int voisinX = 0;
		int voisinY = 0; 
		int orgX = this.getNumberColumn() - 1;
		int orgY = this.getNumberLine() - 1;
		int nbVoisinsContamines = 0;
		
		for(int i = 1; i >= -1; i--){
			//Dans l'axe x, on parcourt les voisins de droite à gauche
			if(col + i < 0){
				voisinX = orgX;
				//System.out.println("Voisin X = " + voisinX);
			}else if(col + 1 > orgX){
				voisinX = 0;
				//System.out.println("Voisin X = " + voisinX);
			}else{
				voisinX = col + i; //De droite à gauche par rapport à la cellule actuelle
				//System.out.println("Voisin X = " + voisinX);
			}
			
			for(int j = -1; j <= 1; j++){
				//Deuxième boucle pour avoir l'axe Y, ce qui permet aussi d'avoir les voisins en diagonale
				if(ligne + j < 0){
					voisinY = orgY;
					//System.out.println("Voisin Y = " + voisinY);
				}else if(ligne + j > orgY){
					voisinY = 0;
					//System.out.println("Voisin Y = " + voisinY);
				}else{
					voisinY = ligne + j;
					//System.out.println("Voisin Y = " + voisinY);
				}
				if(organisme[voisinX][voisinY] == true){
					nbVoisinsContamines += 1;
					//System.out.println("Nombre de voisins contamines = " + nbVoisinsContamines);
				}
			}
		}
		
		if(organisme[col][ligne] == true){
			nbVoisinsContamines -= 1;
		}
		
		return nbVoisinsContamines;
	}

	/*public int calculeNbVoisinsContamines(int col, int ligne) {
		int nbVoisinsContamines = 0;

		if (trouverVoisinNord(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Nord");
		}
		if (trouverVoisinEst(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Est");
		}
		if (trouverVoisinOuest(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Ouest");
		}
		if (trouverVoisinSud(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Sud");
		}
		if (trouverVoisinNordEst(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Nord Est");
		}
		if (trouverVoisinSudEst(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Sud Est");
		}
		if (trouverVoisinSudOuest(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Sud Ouest");
		}
		if (trouverVoisinNordOuest(col, ligne) == true) {
			nbVoisinsContamines += 1;
			//System.out.println("Nord Ouest");
		}

		//System.out.println("Nb voisins : " + nbVoisinsContamines);
		return nbVoisinsContamines;
	}

	// METHODES TROUVER VOISINS
	public boolean trouverVoisinNord(int col, int ligne) {
		boolean etat = false;
		if (ligne - 1 < 0) {
			etat = organisme[col][this.getNumberLine() - 1];
		} else {
			etat = organisme[col][ligne - 1];
		}
		return etat;
	}

	public boolean trouverVoisinSud(int col, int ligne) {
		boolean etat = false;
		if (ligne + 1 > this.getNumberLine() - 1) {
			// etat = organisme[col][ligne % getNumberLine()];
			etat = organisme[col][0];
		} else {
			etat = organisme[col][ligne + 1];
		}
		return etat;
	}

	public boolean trouverVoisinOuest(int col, int ligne) {
		boolean etat = false;
		if (col - 1 < 0) {
			etat = organisme[this.getNumberColumn() - 1][ligne];
		} else {
			etat = organisme[col - 1][ligne];
		}
		return etat;
	}

	public boolean trouverVoisinEst(int col, int ligne) {
		boolean etat = false;
		if (col + 1 > this.getNumberColumn() - 1) {
			etat = organisme[0][ligne];
		} else {
			etat = organisme[col + 1][ligne];
		}
		return etat;
	}

	public boolean trouverVoisinNordEst(int col, int ligne) {
		boolean etat = false;

		if ((col + 1 > this.getNumberColumn() - 1) && (ligne - 1 < 0)) {
			etat = organisme[0][this.getNumberLine() - 1];
		} else if ((col + 1 > this.getNumberColumn() - 1) && (ligne - 1 >= 0)) {
			etat = organisme[0][ligne - 1];
		} else if ((col + 1 <= this.getNumberColumn() - 1) && (ligne - 1 < 0)) {
			etat = organisme[col + 1][this.getNumberLine() - 1];
		} else if((col + 1 <= this.getNumberColumn() - 1) && (ligne - 1 >= 0)){
			etat = organisme[col + 1][ligne - 1];
		}
		return etat;
	}

	public boolean trouverVoisinSudEst(int col, int ligne) {
		boolean etat = false;
		if ((col + 1 > this.getNumberColumn() - 1) && (ligne + 1 > this.getNumberLine() - 1)) {
			etat = organisme[0][0];
		} else if ((col + 1 > this.getNumberColumn() - 1) && (ligne + 1 <= this.getNumberLine() - 1)) {
			etat = organisme[0][ligne + 1];
		} else if ((col + 1 <= this.getNumberColumn() - 1) && (ligne + 1 > this.getNumberLine() - 1)) {
			etat = organisme[col + 1][0];
		} else {
			etat = organisme[col + 1][ligne + 1];
		}
		return etat;
	}

	public boolean trouverVoisinNordOuest(int col, int ligne) {
		boolean etat = false;
		if ((col - 1 < 0) && (ligne - 1 < 0)) {
			etat = organisme[this.getNumberColumn() - 1][this.getNumberLine() - 1];
		} else if ((col - 1 < 0) && (ligne - 1 >= 0)) {
			etat = organisme[this.getNumberColumn() - 1][ligne - 1];
		} else if ((col - 1 >= 0) && (ligne - 1 < 0)) {
			etat = organisme[col - 1][this.getNumberLine() - 1];
		} else {
			etat = organisme[col - 1][ligne - 1];
		}
		return etat;
	}

	public boolean trouverVoisinSudOuest(int col, int ligne) {
		boolean etat = false;
		if ((col - 1 < 0) && (ligne + 1 > this.getNumberLine() - 1)) {
			etat = organisme[this.getNumberColumn() - 1][0];
		} else if ((col - 1 < 0) && (ligne + 1 <= this.getNumberLine() - 1)) {
			etat = organisme[this.getNumberColumn() - 1][ligne + 1];
		} else if ((col - 1 >= 0) && (ligne + 1 > this.getNumberLine() - 1)) {
			etat = organisme[col - 1][0];
		} else {
			etat = organisme[col - 1][ligne + 1];
		}
		return etat;
	}*/

	/*
	 * public int gererDebordements(int col, int ligne){ int resultat = 0;
	 * for(int i = col - 1; i <= col + 1; i++){ for(int j = ligne - 1; j <=
	 * ligne + 1; j++){ if(organisme[i % (this.getNumberColumn() - 1)][j %
	 * (this.getNumberLine() - 1)] == true){ resultat += 1; } } } return
	 * resultat; }
	 */

	/**
	 * Trouve les cellules qui sont infect�es
	 * @return retourne un tableau avec les cellules infect�s
	 */
	public int[][] trouverPosCellulesInfectees() {
		int[][] celPos = new int[this.getNumberColumn()][this.getNumberLine()];


		return celPos;
	}

	
	/**
	 * M�thode interne permettant de g�n�rer l'organisme
	 * @return un tableau de boolean repr�sentant l'organisme.
	 */
		private boolean[][] genererOrganisme() {
		boolean[][] org = new boolean[this.getNumberColumn()][this.getNumberLine()];
		Random r = new Random();
		boolean[][] blinker = pattern.blinker();
		boolean[][] octogone = pattern.octogone();
		boolean[][] oscillateurSeul = pattern.oscillateurSeul();
		boolean[][] planeur = pattern.planeur();
		boolean[][] LWSS = pattern.LWSS();
		boolean[][] HWSS = pattern.HWSS();

		
		int blinkerRandCol = r.nextInt((this.getNumberColumn() - blinker.length)) ;
		int blinkerRandLine = r.nextInt((this.getNumberLine() - blinker[0].length)) ;
		
		int octogoneRandCol = r.nextInt((this.getNumberColumn() - octogone.length)) ;
		int octogoneRandLine = r.nextInt((this.getNumberLine() - octogone[0].length)) ;
		
		int oscillateurSeulRandCol = r.nextInt((this.getNumberColumn() - oscillateurSeul.length));
		int oscillateurSeulRandLine = r.nextInt((this.getNumberLine() - oscillateurSeul[0].length));
		
		int planeurRandCol = r.nextInt((this.getNumberColumn() - planeur.length));
		int planeurRandLine = r.nextInt((this.getNumberLine() - planeur[0].length));
		
		int LWSSRandCol = r.nextInt((this.getNumberColumn() - LWSS.length));
		int LWSSRandLine = r.nextInt((this.getNumberLine() - LWSS[0].length));

		int HWSSRandCol = r.nextInt((this.getNumberColumn() - HWSS.length));
		int HWSSRandLine = r.nextInt((this.getNumberLine() - HWSS[0].length));
		
		
		for(int i = 0; i < blinker.length; i++){
			for(int j = 0; j < blinker[0].length; j++){
				org[blinkerRandCol + i][ blinkerRandLine + j] = blinker[i][j];
				
			}
		}
		
		for(int i = 0; i < octogone.length; i++){
			for(int j = 0; j < octogone[0].length; j++){
				org[octogoneRandCol + i][ octogoneRandLine + j] = octogone[i][j];
				
			}
		}
		
		for(int i = 0; i < oscillateurSeul.length; i++){
			for(int j = 0; j < oscillateurSeul[0].length; j++){
				org[oscillateurSeulRandCol + i][ oscillateurSeulRandLine + j] = oscillateurSeul[i][j];
				
			}
		}
		
		for(int i = 0; i < planeur.length; i++){
			for(int j = 0; j < planeur[0].length; j++){
				org[planeurRandCol + i][ planeurRandLine + j] = planeur[i][j];
				
			}
		}
		
		for(int i = 0; i < LWSS.length; i++){
			for(int j = 0; j < LWSS[0].length; j++){
				org[LWSSRandCol + i][ LWSSRandLine + j] = LWSS[i][j];
				
			}
		}
		
		for(int i = 0; i < HWSS.length; i++){
			for(int j = 0; j < HWSS[0].length; j++){
				org[HWSSRandCol + i][HWSSRandLine + j] = HWSS[i][j];
				
			}
		}
		
		do{
			organisme[r.nextInt(this.getNumberColumn())][r.nextInt(this.getNumberLine())] = true;
		}while(this.getPercentAffected() <= 10);
		
		return organisme;
	}

		/**
		 * Permet de changer l'�tat des cellules en fonction du jeu de conway
		 * @param tempsTotal temps total du jeu
		 */
	public void changerEtatEnFonctionDuJeuDeConway(int tempsTotal) {
		//Organisme avant update
		boolean[][] organisme2 = new boolean[this.getNumberColumn()][this.getNumberLine()];
		
		if (tempsTotal % 230 == 0) {
			for (int i = 0; i < this.getNumberColumn(); i++) {
				for (int j = 0; j < this.getNumberLine(); j++) {
					
					 if (organisme[i][j] == true) {
						if (calculeNbVoisinsContamines(i, j) <= 1 || calculeNbVoisinsContamines(i, j) > 3) {
							organisme2[i][j] = false;
						}else if(calculeNbVoisinsContamines(i, j) == 3 || calculeNbVoisinsContamines(i, j) == 2){
							organisme2[i][j] = true;
						}
					} else if (organisme[i][j] == false){
						if (calculeNbVoisinsContamines(i, j) >= 3) { //Vrai jeu de Conway en commentaire
							organisme2[i][j] = true;
						}else if(calculeNbVoisinsContamines(i, j) < 3 || calculeNbVoisinsContamines(i, j) > 4){
							organisme2[i][j] = false;
						}
					}	
				}
			}
			
			for(int c = 0; c < this.getNumberColumn(); c++){
				for(int l = 0; l < this.getNumberLine(); l++){
					organisme[c][l] = organisme2[c][l];
				}
			}
		}
	}
	
	/**
	 * Permet de formater l'organisme pour un format stockable dans la base de donn�e.
	 * @return une cha�ne de caract�res de F (saine), T (infect�), | (s�parateur de lignes)
	 */
	public String organismeFormatedDatabase(){
		String result = new String();

		for (int i = 0; i < this.organisme.length; i++) {
		    for (int j = 0; j < this.organisme[i].length; j++) {
		    	if(this.organisme[i][j] == true){
		    		result += 'T';
		    	}
		    	else{
		    		result += 'F';
		    	}
		    }
		    result += '|'; // Permet de d�limiter les diff�rentes lignes.
		}
				
		return result;
	}
	
	

	
	
	/**
	 * Override de la m�thode toString affichant dans la console l'�tat de toutes les cellules (utile pour le debug)
	 */
	public String toString(){
		for (int i = 0; i < organisme.length; i++) {
			for (int j = 0; j < organisme[0].length; j++) {
				System.out.println("cell["+ i + "][" + j + "] : " + organisme[i][j]);
			}
		}
		
		return "";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}


}