package com.helmo.cellsoflife.models;

import java.util.concurrent.TimeUnit;

public class Chronometre {
	private int ms;
	
	/**
	 * Instancie un chronom�tre avec sa valeur par d�faut.
	 */
	public Chronometre(){
		this.ms = 0;
	}
	
	/**
	 * Instancie un chronom�tre avec une valeur de d�but personnalis�e.
	 * @param tempsDeJeu temps en milliseconde
	 */
	public Chronometre(int tempsDeJeu) {
		this.ms = tempsDeJeu;
	}

	/**
	 * Permet d'ajouter un temps personnalis�e au chronom�tre
	 * @param elapsedTimeSinceLastUpdateInMillis 
	 */
	public void ajouterTemps(int elapsedTimeSinceLastUpdateInMillis){
		ms += elapsedTimeSinceLastUpdateInMillis;
	}
	
	/**
	 * Retourne le temps total du chronom�tre en ms.
	 * @return temps total en ms.
	 */
	public int getTotalTimeInMillis(){
		return ms;
	}

	/**
	 * R�initialise le chronom�tre � 0.
	 */
	public void clearChrono(){
		this.ms = 0;
	}

	/**
	 * Retourne le chronom�tre format�e
	 * @return un String dans le format HH:MM:SS.
	 */
	public String formatTime(){
		String result = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(ms),
			    TimeUnit.MILLISECONDS.toMinutes(ms) % TimeUnit.HOURS.toMinutes(1),
			    TimeUnit.MILLISECONDS.toSeconds(ms) % TimeUnit.MINUTES.toSeconds(1));
		
		return result;
	}
}
