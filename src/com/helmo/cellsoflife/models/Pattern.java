package com.helmo.cellsoflife.models;

public class Pattern {
	private boolean[][] cells;
	
	public Pattern(){
		
	}
	
	/**
	 * Constructeur d'un Blinker
	 * @return un tableau de boolean 3x3 d'un Blinker
	 */
	public boolean[][] blinker(){
		cells = new boolean[3][3];
		cells[1][0] = true;
		cells[1][1] = true;
		cells[1][2] = true;
		return cells;
	}
	
	/**
	 * Retourne un oscillateur seul
	 * @return un tableau de boolean 11 x 11 d'un oscillateur seul
	 */
	public boolean[][] oscillateurSeul(){
		cells = new boolean[11][11];
		cells[0][5] = true;
		cells[1][4] = true;
		cells[1][5] = true;
		cells[1][6] = true;
		cells[2][2] = true;
		cells[2][3] = true;
		cells[2][4] = true;
		cells[2][6] = true;
		cells[2][7] = true;
		cells[2][8] = true;
		cells[3][2] = true;
		cells[3][8] = true;
		cells[4][1] = true;
		cells[4][2] = true;
		cells[4][8] = true;
		cells[4][9] = true;
		cells[5][0] = true;
		cells[5][1] = true;
		cells[5][9] = true;
		cells[5][10] = true;
		cells[6][1] = true;
		cells[6][2] = true;
		cells[6][8] = true;
		cells[6][9] = true;
		cells[7][2] = true;
		cells[7][8] = true;
		cells[8][2] = true;
		cells[8][3] = true;
		cells[8][4] = true;
		cells[8][6] = true;
		cells[8][7] = true;
		cells[8][8] = true;
		cells[9][4] = true;
		cells[9][5] = true;
		cells[9][6] = true;
		cells[10][5] = true;
		
		return cells;
	}

	/**
	 * Constructeur permettant de retourner un Octogone.
	 * @return un tableau de boolean 6x6 représentant un octogone
	 */
	public boolean[][] octogone(){
		cells = new boolean[6][6];
		cells[0][1] = true;
		cells[0][2] = true;
		cells[0][3] = true;
		cells[0][4] = true;
		cells[1][0] = true;
		cells[1][3] = true;
		cells[1][2] = true;
		cells[1][5] = true;
		cells[2][0] = true;
		cells[2][1] = true;
		cells[2][4] = true;
		cells[2][5] = true;
		cells[3][0] = true;
		cells[3][1] = true;
		cells[3][4] = true;
		cells[3][5] = true;
		cells[4][0] = true;
		cells[4][2] = true;
		cells[4][3] = true;
		cells[4][5] = true;
		cells[5][1] = true;
		cells[5][2] = true;
		cells[5][3] = true;
		cells[5][4] = true;

		return cells;
	}
	
	/**
	 * Constructeur permettant de retourner un planeur
	 * @return tableau de boolean 3x3 représentant un planeur
	 */
	public boolean[][] planeur(){
		cells = new boolean[3][3];
		cells[1][0] = true;
		cells[2][1] = true;
		cells[0][2] = true;
		cells[1][2] = true;
		cells[2][2] = true;
		
		return cells;
	}
	
	/**
	 * Constructeur permettant de retourner un LWSS
	 * @return tableau de boolean 5x4 représentant un LWSS
	 */
	public boolean[][] LWSS(){
		cells = new boolean[5][4];
		cells[0][0] = true;
		cells[3][0] = true;
		cells[4][1] = true;
		cells[0][2] = true;
		cells[4][2] = true;
		cells[1][3] = true;
		cells[2][3] = true;
		cells[3][3] = true;
		cells[4][3] = true;
		
		return cells;
	}
	
	/**
	 * Constructeur permettant de retourner un HWSS
	 * @return tableau de boolean 7x5 représentant un HWSS
	 */
	public boolean[][] HWSS(){
		cells = new boolean[7][5];
		cells[2][0] = true;
		cells[3][0] = true;
		cells[0][1] = true;
		cells[5][1] = true;
		cells[6][2] = true;
		cells[0][3] = true;
		cells[6][3] = true;
		cells[1][4] = true;
		cells[2][4] = true;
		cells[3][4] = true;
		cells[4][4] = true;
		cells[5][4] = true;
		cells[6][4] = true;
		
		return cells;
	}
}
