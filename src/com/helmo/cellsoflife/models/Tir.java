package com.helmo.cellsoflife.models;

public class Tir extends Entite {

	private int positionX, positionY, msShoted, largeurTir, hauteurTir;
	private int vitesse;
	
	
	/**
	 * Constructeur permettant de cr�er un tir
	 * @param positionX position en X du tir
	 * @param positionY position en Y du tir
	 * @param msShoted  temps de jeu (en ms) o� le tir a �t� effectu�
	 */
	public Tir(int positionX, int positionY, int msShoted){
		this.positionX = positionX;
		this.positionY = positionY;
		this.msShoted = msShoted;
		this.vitesse = 1;
	}
	
	
	/**
	 * Permet de d�placer le tir
	 */
	@Override
	public void seDeplacer() {
		this.positionY -= this.vitesse;
	}
	
	/**
	 * Permet de r�cup�rer la position en X du tir
	 * @return int de la position en X du tir
	 */
	public int getPositionX() {
		return positionX;
	}


	/**
	 * Permet de d�finir la position en X du tir
	 * @param positionX position en X du tir
	 */
	public void setPositionX(int positionX) {
		this.positionX = positionX;
	}


	/**
	 * Permet de r�cup�rer la position en Y du tir 
	 * @return int repr�sentant la position Y du tir
	 */
	public int getPositionY() {
		return positionY;
	}


	/**
	 * Permet de d�finir la postion en Y du tir
	 * @param positionY position en Y du tir
	 */
	public void setPositionY(int positionY) {
		this.positionY = positionY;
	}


	/**
	 * Retourne le temps total du jeu en ms o� le tir a �t� effectu�
	 * @return int repr�sentant le temps total du jeu quand le tir � �t� effectu�
	 */
	public int getmsShoted(){
		return this.msShoted;
	}
	
	/**
	 * Override de toString permettant de retourner les infos du tir (pour du debug live)
	 */
	public String toString(){
		return "posX : " + this.positionX + " | posY : " + this.positionY + " | msShoted: " + this.msShoted;
	}

}
