package com.helmo.cellsoflife.models;



import com.helmo.cellsoflife.Program;

public class Vaisseau extends Entite {
	private int cadenceTir = 100, positionX, positionY, vitesse, hauteur, largeur; // 100 ms, bjr le ping... :D
	
	
	/**
	 * Constructeur par d�faut placant le vaisseau au milieu de l'�cran.
	 */
	public Vaisseau(){
		this.vitesse = 1;
		this.hauteur = 50;
		this.largeur = 50;
		this.positionX = (int) (0 + ((Program.WIDTH * 0.35) + (this.largeur * 0.5) * 0.5));
	}
	
	/**
	 * Constructeur alternatif permettant la position en X et en Y personnalis�e du vaisseau
	 * @param vaisseauPositionX position en X du vaisseau
	 * @param vaisseauPositionY position en Y du vaisseau (useless...)
	 */
	public Vaisseau(int vaisseauPositionX, int vaisseauPositionY) {
		this.vitesse = 1;
		this.hauteur = 50;
		this.largeur = 50;
		this.positionX = vaisseauPositionX;
		this.positionY = vaisseauPositionY;
	}

	/*
	 * 
	 * On verra si on a le temps de g�n�rer plusieurs "types de vaisseau"
	 *
	 * 
	public Vaisseau(int typeVaisseau){
		switch (typeVaisseau) {
		case 1:
			this.imageVaisseau = 2;
			this.vitesse = 2;
			this.hauteur = 50;
			this.largeur = 50;
			this.positionX = (int)((Program.WIDTH * 0.66) - this.largeur * 0.5);
			break;

		default:
			new Vaisseau();
			this.hauteur = 50;
			this.largeur = 50;
			this.positionX = (int)((Program.WIDTH * 0.66) - this.largeur * 0.5);
			break;
		}
	}
	
	public int getImageVaisseau(){
		return this.imageVaisseau;
	}
*/
	
	/**
	 * Permet de d�placer le vaiseau de la vitesse par d�faut du vaisseau
	 */
	@Override
	public void seDeplacer() {
		this.positionX += this.vitesse;
	}
	
	/**
	 * Retourne la position en X du vaisseau
	 * @return position en X du vaisseau (int)
	 */
	public int getPositionX(){
		return positionX;
	}
	
	/**
	 * Retourne la vitesse du vaisseau 
	 * @return vitesse du vaisseau
	 */
	public int getVitesse(){
		return this.vitesse;
	}
	
	/**
	 * Retourne la largeur du vaisseau
	 * @return largeur du vaisseau
	 */
	public int getLargeur(){
		return this.largeur;
	}

	/**
	 * Retourne la hauteur du vaisseau 
	 * @return  hauteur du vaisseau 
	 */
	public int getHauteur() {
		// TODO Auto-generated method stub
		return this.hauteur;
	}
	
	/**
	 * Permet de d�placer la vaisseau avec une vitesse personnalis�e
	 * @param i vitesse personnalis�e de d�placement en X du vaisseau
	 */
	public void seDeplacer(int i) {
		this.positionX += i;		
	}

	/**
	 * Retourne la position en Y du vaisseau
	 * @return position en Y du vaisseau
	 */ 
	public float getPositionY() {
		return (Program.HEIGHT - (this.hauteur + 5));
	}

}
