package com.helmo.cellsoflife;
import java.util.ArrayList;

import com.helmo.cellsoflife.models.Organisme;
import com.helmo.cellsoflife.models.Tir;
import com.helmo.cellsoflife.models.Vaisseau;

import helmo.nhpack.NHDatabaseSession;
import helmo.nhpack.db.ConnectionConfig;
import helmo.nhpack.db.SqlServerConnectionConfig;

public class CellsDatabase {
	private ConnectionConfig connexion = new SqlServerConnectionConfig().withHost("192.168.128.18")
            .withDatabase("in15b1095")
            .withUserName("in15b1095")
            .withPassword("5583");

	
	/**
	 * Constructeur permettant l'instanciation d'une connexion � la base de donn�e.
	 */
	public CellsDatabase(){
		
	}

	
	/**
	 * M�thode interne permettant de retourner un tableau (String) 
	 * comportant uniquement les scores des 10 meilleures parties
	 * @return Tableau 2d de String avec uniquement le score des 10 meilleures parties
	 */
	private String[][] getDixMeilleuresScoreOnly(){
		String[][] result;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery("SELECT TOP 10 score FROM AI_jeu WHERE LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) <= 9 OR LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) >= 360 ORDER BY score DESC;");
		}
		

		return result;
	}
	
	/**
	 * M�thode interne permettant de retourner un tableau (String) 
	 * comportant uniquement les temps des 10 meilleures temps de jeu
	 * @return Tableau 2d de String avec uniquement le temps de jeu des 10 meilleures parties
	 */
	private String[][] getDixMeilleuresTempsOnly(){
		String[][] result;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery("SELECT TOP 10 temps_jeu FROM AI_jeu WHERE LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) <= 9 OR LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) >= 360 ORDER BY temps_jeu ASC;");
		}
			

		return result;
	}
	
	/**
	 * Retourne un tableau 2d d'INT avec les 10 meilleures scores enregistr�s
	 * @return tableau des dix meilleures scores
	 */
	public int[] dixMeilleuresScoreOnly(){
		int[] result = new int[10];
		String[][] tab = getDixMeilleuresScoreOnly();
		
		for(int i = 0; i < tab.length; i++){
			for (int j = 0; j < tab[0].length; j++) {
				result[i] = Integer.parseInt(tab[i][j]);
			}
		}
		
		
		return result;
		
 	}
	
	/**
	 * Retourne un tableau 2d d'INT avec les 10 meilleures temps de jeu enregistr�s
	 * @return tableau des dix meilleures temps de jeu
	 */
	public int[] dixMeilleuresTempsOnly(){
		int[] result = new int[10];
		String[][] tab = getDixMeilleuresTempsOnly();
		
		tab.toString();
		
		for(int i = 0; i < tab.length; i++){
			for (int j = 0; j < tab[0].length; j++) {
				result[i] = Integer.parseInt(tab[i][j]);
			}
		}
		
		return result;
	}
	
	
	
	/**
	 * Retourne les dix meilleures scores stock� en base de donn�es avec toutes les informations n�cessaires
	 * @return tableau (String) 2D avec les 10 meilleures scores (informations d�taill�es)
	 */
	public String[][] dixMeilleuresScores() {
		String[][] result;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery("SELECT TOP 10 nom_joueur, score, temps_jeu FROM AI_jeu WHERE LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) <= 9 OR LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) >= 360 ORDER BY score DESC;");
		}
		

		return result;
	}
	
	/**
	 * Retourne les dix meilleures temps de jeu stock� en base de donn�es avec toutes les informations n�cessaires
	 * @return tableau (String) 2D avec les 10 meilleures temps de jeu (informations d�taill�es)
	 */
	public String[][] dixMeilleuresTemps(){
		String[][] result;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery("SELECT TOP 10 nom_joueur, score, temps_jeu FROM AI_jeu WHERE LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) <= 9 OR LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) >= 360 ORDER BY temps_jeu ASC;");
		}

		return result;
	}
	
	
	/**
	 * Permet de sauvegarder une partie dans la base de donn�e 
	 * @param jeu instance de Jeu de la partie que l'on d�sire sauvegarder
	 */
	public void saveGame(Jeu jeu, String username){
		int idJeu;
		float score = jeu.getScore();
		ArrayList<Tir> listeTirs = jeu.getTirs();
		int tempsTotal = jeu.getTotalTime();
		int vaisseauPositionX = jeu.getVaisseau().getPositionX();
		int vaisseauPositionY = (int)jeu.getVaisseau().getPositionY();
		
		
		String organisme = jeu.getOrganisme().organismeFormatedDatabase();

		
		try(NHDatabaseSession session = new NHDatabaseSession(connexion)) {

            // On commence par enregistrer le jeu (la partie)
            int tupleModified = session.createStatement("INSERT INTO AI_jeu(score, temps_jeu, nom_joueur, organisme) VALUES(:score, :temps_jeu, :nom_joueur, :organisme)")
            		.bindParameter(":score", score)
                    .bindParameter(":temps_jeu", tempsTotal)
                    .bindParameter(":nom_joueur", username)
                    .bindParameter(":organisme", organisme)
                    .executeUpdate();
            
           idJeu = getLastJeuID();
            
            
            session.openTransaction();          
            if(tupleModified >= 0){
            	for (int i = 0; i < listeTirs.size(); i++) {
					Tir t = listeTirs.get(i);
					
					tupleModified += session.createStatement("INSERT INTO AI_Entite(positionX, positionY, type_entite, id_jeu, msShoted) VALUES(:posX, :posY, :type_entite, :id_jeu, :msShoted)")
	                		.bindParameter(":posX", t.getPositionX())
	                		.bindParameter(":posY", t.getPositionY())
	                        .bindParameter(":type_entite", "T")
	                        .bindParameter(":id_jeu", idJeu)
	                        .bindParameter(":msShoted", t.getmsShoted())
	                        .executeUpdate();
				}
            	
            	tupleModified += session.createStatement("INSERT INTO AI_Entite(positionX, positionY, type_entite, id_jeu) VALUES(:posX, :posY, :type_entite, :id_jeu)")
                		.bindParameter(":posX", vaisseauPositionX)
                		.bindParameter(":posY", vaisseauPositionY)
                        .bindParameter(":type_entite", "V")
                        .bindParameter(":id_jeu", idJeu)
                        .executeUpdate();
            }
            
           
            if(tupleModified < 0) { //Des probl�mes sont survenus
                System.err.println(session.getLastError());
                session.rollback();
            } else {
            	System.out.println("Partie correctement enregistr�e!");
                session.commit();
            }
        }

	}
	
	/**
	 * Retourne le dernier id_jeu se trouvant dans la base de donn�e
	 * @return
	 */
	private int getLastJeuID() {
		String[][] result;
		int toReturn = 0;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery("SELECT TOP 1 id_jeu FROM AI_jeu ORDER BY id_jeu DESC;");
		}
		
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				toReturn = Integer.parseInt(result[i][j]);
			}
		}

			return toReturn;
	}


	/**
	 * M�thode permettant de charger la partie active stock�e en base de donn�e
	 * @return une instance de Jeu avec les informations de la partie active stock�e en base de donn�e
	 */
	public Jeu loadGame(){
		int score = 0;
		Organisme organisme = null;
		ArrayList<Tir> listeTirs = new ArrayList<>();
		int tempsTotal = 0;
		int vaisseauPositionX = 0;
		int vaisseauPositionY = 0;
		Vaisseau vaisseau;
		

			// On r�cup�re l'ID du jeu ouvert, maintenant que l'on sait que l'on a une partie en cours...
			int idOpenedGame = 0;
			try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
				String[][] result = session.executeQuery(
						"SELECT TOP 1 id_jeu "
						+ "FROM AI_jeu "
						+ "WHERE LEN(REPLACE(organisme, 'F', '')) BETWEEN 9 AND 360"
						+ "ORDER BY id_jeu DESC;");
				
				idOpenedGame = Integer.parseInt(result[0][0]);
			}
			
			System.out.println("id open game : " + idOpenedGame);
			
			// On charge le score de la partie et son organisme.
			try(NHDatabaseSession session = new NHDatabaseSession(connexion)) {


	            String[][] tuples = session.createStatement("SELECT score, organisme, temps_jeu "+
	                                                            "FROM AI_jeu WHERE id_jeu =  :id_jeu")
	            		.bindParameter(":id_jeu", idOpenedGame)
	                                                              .executeQuery();
	                
	            // Affectation de score, de l'organisme et du temps de jeu total.
	            score = Integer.parseInt(tuples[0][0]);
	            System.out.println("ID GAME TO LOAD: " + idOpenedGame);
	            organisme = decodeOrganisme(tuples[0][1]);
	            tempsTotal = Integer.parseInt(tuples[0][2]);
	            
	            // Chargement du vaisseau
	            tuples = session.createStatement("SELECT positionX, positionY FROM AI_Entite WHERE type_entite = :type_entite AND id_jeu = :id_jeu")
	            		.bindParameter(":type_entite", "V")
	            		.bindParameter(":id_jeu", idOpenedGame)
	            		.executeQuery();
	            
	           vaisseauPositionX = Integer.parseInt(tuples[0][0]);
	           vaisseauPositionY = Integer.parseInt(tuples[0][1]);
	           
	           
	           
	           // Chargement des tirs
	           String[][] tuplesTirs = session.createStatement("SELECT positionX, positionY, msShoted FROM AI_Entite WHERE type_entite = :type_entite AND  id_jeu =  :id_jeu")
	            		.bindParameter(":type_entite", "T")
	            		.bindParameter(":id_jeu", idOpenedGame)
	            		.executeQuery();
	           
	           listeTirs = generateArrayListTirs(tuplesTirs);
	           
	        }
		
			// si aucune partie en db, organisme est null et le temps est n�cessairement null
		if(organisme == null || tempsTotal <= 0){
			throw new IllegalArgumentException("Aucune partie dans la base de donn�e");
		}
		
		return new Jeu(organisme, tempsTotal, listeTirs, score, vaisseauPositionX, vaisseauPositionY);
	}
	
	
	/**
	 * M�thodes qui g�n�re une liste des tirs avec un tableau de String[][].
	 * @param tuples : AVEC 3 COLONNES (col1: positionX du tir, col2: positionY du tir, col3: msShoted du tir)
	 * @return une ArrayList de Tirs de la partie que l'on d�sire charg�e
	 */
	private ArrayList<Tir> generateArrayListTirs(String[][] tuples) {
		
		if(tuples.length == 0){
			System.out.println("Tableau vide...");
			return new ArrayList<Tir>();
		}
		
		
		if(tuples[0].length != 3){
			throw new IllegalArgumentException("Le tableau doit contenir 3 colonnes: position X, position Y et le temps (en ms) du tir");
		}
		
		
		ArrayList<Tir> result = new ArrayList<>();
		
		System.out.println("Taille de tuples de tirs: " + tuples.length);
		for (int i = 0; i < tuples.length; i++) {
			result.add(new Tir(Integer.parseInt(tuples[i][0]), Integer.parseInt(tuples[i][1]), Integer.parseInt(tuples[i][2])));
		}

		return result;
	}


	/**
	 * Permet de savoir si une partie active est actuellement stock�e dans la base de donn�e
	 * @return true si une partie active est stock�e, false si pas de partie active stock�e
	 */
	public boolean isActiveGameStoredInDatabase(){
		/*
		 * On doit v�rifier si il y a bien une partie de disponible dans la base de donn�e;
		 * Pour cela nous devons chercher dans toutes les parties
		 * Si un organisme corresponds au crit�res suivants:
		 * 		- Un nombre T plus grand que 1 et plus petit que 40.
		 * 
		 * 
		 * Si nous avons un organisme qui correspond au crit�re, on retourne true, sinon false.
		 * */
		String[][] result;
		boolean toReturn = false;
		int valueOfCaseInResult = Integer.MIN_VALUE;
		try (NHDatabaseSession session = new NHDatabaseSession(this.connexion)) {
			result = session.executeQuery(
					"SELECT TOP 1 id_jeu "
					+ "FROM AI_jeu "
					+ "WHERE LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) > 9 AND LEN(organisme) - (LEN(organisme) - LEN(replace(organisme, 'F', '')) + 30) < 360"
					+ "ORDER BY id_jeu DESC;");
		}
		// On v�rifie si la query a retourn� un r�sultat. Si oui = on a une partie, donc on retourne true.
		if(result.length > 0){
			toReturn = true;
		}
		return toReturn;
	}
	
	
	
	/**
	 * R�cup�re un organisme au format String et le retourne en un objet Organisme.
	 * @param organismeString (Organisme format� pour �tre stock�e dans la base de donn�e)
	 * @return un objet Organisme correspondant � la cha�ne donn�e en param�tre
	 */
	public Organisme decodeOrganisme(String organismeString){
		// Fonctionne uniquement avec des matrices carr�es.
		int nbLignes = (int) Math.sqrt(organismeString.length());
		int nbColonnes = nbLignes;
		int tmpStringStart = 0, tmpStringEnd = 0;
		
		Organisme result = new Organisme(nbLignes, nbColonnes, true);
		
				
		int i = 0; // Stocke la ligne actuelle
		int k = 0;
		for (int j = 0; j < organismeString.length(); j++) {
			
			switch (organismeString.charAt(j)) {
			case 'T':
				result.setCells(i, k, true);
				k++;
				break;
			case 'F':
				result.setCells(i, k, false);
				k++;
				break;
			case '|':
				i++;
				k = 0;
				break;
			default:
				throw new IllegalArgumentException("La chaine String de l'organisme ne correspond pas � notre design.");
			}
		}	
		return result;
	}


	
}