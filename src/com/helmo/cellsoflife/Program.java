package com.helmo.cellsoflife;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;


public class Program{
	public final static int WIDTH = 1024;
	public final static int HEIGHT = 768;


	public static void main(String[] args) {
		try
		{
			AppGameContainer appgc;
			appgc = new AppGameContainer(new CellsGame());
			appgc.setShowFPS(true);
			appgc.setDisplayMode(WIDTH, HEIGHT, false);
			appgc.start();
		}
		catch (SlickException ex)
		{
			Logger.getLogger(CellsGame.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
}
