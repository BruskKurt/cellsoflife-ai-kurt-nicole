package com.helmo.cellsoflife.scenes;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.helmo.cellsoflife.CellsDatabase;
import com.helmo.cellsoflife.Jeu;
import com.helmo.cellsoflife.Program;

public class GameScene extends Scene{
	private Jeu jeu = null; // Par défaut, notre jeu est null.
	private double width; // Pour stocker la taille de la hauteur et de la largeur de la fenêtre.
	int hauteurCellule, largeurCellule;
	double pourcentageInfection;
	Image imgVaisseau, imgTir, fond, imgVaisseau2;
	private int tempsTotal = 0;
	private CellsDatabase db;


	public GameScene(Jeu test) {
		this.jeu = test;
	}

	public GameScene() {
	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Scene result = new GameScene();
			
			if(gc.getInput().isKeyPressed(Input.KEY_P)){
				// Si on appuye sur Pause, on stocke directement la partie dans la base de donnée.
				this.db.saveGame(jeu, "NULL");
				result = new PauseScene();
			}
			// dans le cas on on appuie pas sur pause, on continue le jeu.
			else{

				if(gc.getInput().isKeyPressed(Input.KEY_LEFT) || gc.getInput().isKeyDown(Input.KEY_LEFT) ){
					jeu.deplacerVaisseau("left");
				}
				else if(gc.getInput().isKeyPressed(Input.KEY_RIGHT) || gc.getInput().isKeyDown(Input.KEY_RIGHT) ){
					jeu.deplacerVaisseau("right");
				}
				else if(gc.getInput().isKeyPressed(Input.KEY_SPACE) || gc.getInput().isKeyDown(Input.KEY_SPACE)){
					jeu.tirerMedicament(elapsedTimeSinceLastUpdateInMillis);
				}

				
				/**
				 * Fin du jeu selon le pourcentage d'infection.
				 * => On affiche EndGameScene avec l'instance du jeu (infos) et le message à afficher selon
				 * que c'est gagné ou perdu. 
				 */
				if(jeu.getPercentAffected() <= 1){
					// System.out.println("crois-t-il que c'est gagné?");
					result =  new EndGameScene(this.jeu, "Bravo, vous avez gagné la partie!");
				}
				else if(jeu.getPercentAffected() >= 40){
					// System.out.println("crois-t-il que c'est perdu?");
					result = new EndGameScene(this.jeu, "What a loser! C'est Game Over pour toi, réessaie!");
				}

				
				
				jeu.moveShots();
				jeu.addTime(elapsedTimeSinceLastUpdateInMillis);
				pourcentageInfection = jeu.getPercentAffected();
				jeu.detectHit();
				tempsTotal += elapsedTimeSinceLastUpdateInMillis;
				
				// Update de l'organisme.
				jeu.jeuDeConway(tempsTotal);
			}

		return result;
	}

	/*
	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
	}*/

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		System.out.println("Arrivée de la GameScene");
		db = new CellsDatabase();
		this.width = Program.WIDTH * 0.66;
		
		// Je vérifie si jeu est null ou pas (s'il ne l'est pas, c'est que nous l'avons load d'une partie saved)
		if(this.jeu == null){
			this.jeu = new Jeu();
		}
		else{
			System.out.println("Game loaded, let's go !");
		}
		
		
		
		// Création des graphics pour le jeu.
		imgVaisseau = new Image("com/helmo/cellsoflife/ressources/vaisseau-default.png");
		imgTir = new Image("com/helmo/cellsoflife/ressources/medic-pack.png");
		fond = new Image("com/helmo/cellsoflife/ressources/gamescenebg.jpg");
		
		/* On définit la taille des cellules selon: 
		 * LARGEUR CELULE - width divisé par le nombre de lignes de l'organisme
		 * HAUTEUR CELULE - height moins la taille du vaisseau divisé par le nbr de colonnes de l'organisme
		 * */
		this.largeurCellule = (int)(width / jeu.getOrganismeLine());
		this.hauteurCellule = (int) (Program.HEIGHT - jeu.getHauteurVaisseau()) / jeu.getOrganismeColumn();
		// this.hauteurCellule = (Program.HEIGHT - jeu.getHauteurVaisseau()) / jeu.getOrganismeColumn();
		// COMMENT 66: retourne +1 pixel, donc faisons en sorte d'avoir un carré exacte et dessinons comme ça...
		
		
		/* On reprend la hauteur et largeur de la fenêtre de jeu que l'on multiplie 
		 par 0,75 pour avoir les 1/3 de la fenêtre dédié au jeu */
		
				
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		// Création de l'image du vaisseau et du tir
		
		painter.drawImage(fond, 0, 0);
		
		
		// Affichage du vaisseau de jeu
		painter.drawImage(imgVaisseau, jeu.positionVaisseau(), jeu.positionVaisseauY());
		
		
		// Affichage du menu à droite
		painter.setColor(Color.lightGray);
		painter.drawString("Infecté à : " + pourcentageInfection + "%", (float) (width ), 15);
		painter.drawString("Temps de jeu: " + (jeu.getFormatedChrono()), (float) (width ), 45);
		painter.drawString("Score du jeu: " + jeu.getScore(), (float) (width), 75);
		painter.drawString("Nombre de tirs: " + jeu.getTirs().size(), (float) (width), 250);
		painter.drawString("-------------------- ", (float) (width), 105);
		painter.drawString("Commandes du jeu", (float) (width), 135);
		painter.drawString("Déplacements: <- | ->", (float) (width), 165);
		painter.drawString("Tirer medic.: ESPACE", (float) (width), 195);
		painter.drawString("Mettre en pause: P", (float) (width), 225);
		painter.setColor(Color.white);
		
		
		
		// Affichage des cellules infectés.
		for (int i = 0; i < jeu.getOrganismeLine(); i++) {
			for (int j = 0; j < jeu.getOrganismeColumn(); j++) {
				if(jeu.getStatusCell(i, j) == true){
					painter.setColor(Color.orange);

					painter.fillOval(i*largeurCellule, j*hauteurCellule, largeurCellule-1, hauteurCellule-1);
				}
				painter.setColor(Color.white);
			}
		}
		// Affichage des tirs
			for(int i = 0; i < jeu.getTirs().size(); i++){
				painter.drawImage(imgTir, jeu.getTirs().get(i).getPositionX(), jeu.getTirs().get(i).getPositionY());
		}
	}
}
