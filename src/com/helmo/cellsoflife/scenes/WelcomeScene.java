package com.helmo.cellsoflife.scenes;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.helmo.cellsoflife.CellsDatabase;
import com.helmo.cellsoflife.Jeu;


public class WelcomeScene extends Scene {

	// Variables locales
	private Image fond;
	private CellsDatabase conn = new CellsDatabase();
	private String partieEnCoursStatus = "lol";


	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		Scene result = new WelcomeScene(); // result est un objet Scene que l'on retournera selon la touche press�e
		/* 
		 * le if/elseif/else en attendant le possible switch/case 
		 * ==== G�re les touches du menu et op�re selon la touche press�e ====
		 * */
		if(gc.getInput().isKeyPressed(Input.KEY_A)){
			// Si la touche a est press�e, on affecte � result une instance de RankingScene
			result = new RankingScene();
			System.out.println("Afficher classement");
		}
		else if(gc.getInput().isKeyPressed(Input.KEY_C)){
			if(this.conn.isActiveGameStoredInDatabase()){
				Jeu test = conn.loadGame();
				result = new GameScene(test);
			}
			else{
				System.out.println("rien � charger.");
			}

		}
		else if(gc.getInput().isKeyPressed(Input.KEY_N)){
			result = new GameScene();

		}
		else if(gc.getInput().isKeyPressed(Input.KEY_Q)){
			gc.exit(); // On utilise la fonction exit de Slick2D pour quitter le jeu...
		}
		
		
		return result;
	}

	/* [POSSIBLE DUPLICATE OF DRAW METHOD] 
	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
	}
	*/

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		/* 
		 * Draw basique du fond et du texte.
		 * */
		fond = new Image("com/helmo/cellsoflife/ressources/pausebg.png");
		painter.setColor(Color.white);
		painter.drawImage(fond, 0, 0);
		painter.drawString("Bienvenue sur CellsOfLife! Que d�sirez-vous faire?", 20, 20);
		painter.drawString("(N)ouvelle partie", 50, 40);
		painter.drawString("(C)ontinuer partie", 50, 60);
		
		painter.setColor(Color.yellow);
		if(conn.isActiveGameStoredInDatabase()){
			this.partieEnCoursStatus = "[!][!] Une partie est en cours et est pr�te � �tre charg�e [!][!]";
			painter.drawString("D�marrer une nouvelle partie �crasera celle en cours!", 250, 40);
		}
		else{
			this.partieEnCoursStatus = "Aucune partie n'est en cours de jeu.";
		}
		painter.drawString(this.partieEnCoursStatus, 250, 60);
		
		painter.setColor(Color.white);
		painter.drawString("(A)fficher classement", 50, 80);
		painter.drawString("(Q)uitter le jeu", 50, 100);
	}

}
