package com.helmo.cellsoflife.scenes;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.helmo.cellsoflife.CellsDatabase;
import com.helmo.cellsoflife.models.Chronometre;

public class RankingScene extends Scene {
	private Image fond;
	private CellsDatabase database;
	private String[][] meilleurScore, meilleurTemps;
	
	
	public RankingScene(){
		
	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		// TODO Auto-generated method stub
		Scene result =  new RankingScene();
		
		if(gc.getInput().isKeyPressed(Input.KEY_R)){
			// Si la touche a est press�e, on affecte � result une instance de RankingScene
			result = new WelcomeScene();
		}
		return result;
	}
	/*
	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		// TODO Auto-generated method stub
		
	}*/

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		database = new CellsDatabase();
		meilleurScore = database.dixMeilleuresScores();
		meilleurTemps = database.dixMeilleuresTemps();
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		painter.setColor(Color.white);
		fond = new Image("com/helmo/cellsoflife/ressources/pausebg.png");
		fond.draw();
		painter.drawString("(R)etour au menu principal", 50, 50);
		
		
		painter.drawString("CLASSEMENT DES MEILLEURS SCORES", 50, 100);
		
		painter.drawString("CLASSEMENT DES MEILLEURS TEMPS", 600, 100);

		
		painter.drawString("Rang",   50, 150);
		painter.drawString("Joueur", 150, 150);
		painter.drawString("Score", 250, 150);
		painter.drawString("Temps", 350, 150);
		
		
		painter.drawString("Rang",   600, 150);
		painter.drawString("Joueur", 700, 150);
		painter.drawString("Score", 800, 150);
		painter.drawString("Temps", 900, 150);
		
		
		
		painter.setColor(Color.yellow);
		int positionxDefaut = 50;
		int positionyDefaut = 200;
		Chronometre ms;
		
		if(this.meilleurScore.length <= 0){
			painter.drawString("Aucune partie disponible dans le classement", positionxDefaut, positionyDefaut);
		}
		else{
			for (int i = 0; i < meilleurScore.length; i++) {
				ms = new Chronometre(Integer.parseInt(meilleurScore[i][2]));
				painter.drawString("" + (i + 1), (float) positionxDefaut, positionyDefaut);
				for (int j = 0; j < meilleurScore[0].length; j++) {
					
					
					if(j != 2){
					painter.drawString("" + meilleurScore[i][j], (float) positionxDefaut + 100, positionyDefaut);
					}
					else{
						painter.drawString(ms.formatTime(), (float) positionxDefaut + 100, positionyDefaut);
					}
					
					positionxDefaut += 100;
				}
				positionxDefaut = 50;
				positionyDefaut += 50;
			}
			positionyDefaut = 200;
		}
		
		positionxDefaut = 600;
		if(this.meilleurTemps.length <= 0){
			painter.drawString("Aucune partie disponible dans le classement", positionxDefaut, positionyDefaut);
		}
		else{
			for (int i = 0; i < meilleurTemps.length; i++) {
				ms = new Chronometre(Integer.parseInt(meilleurTemps[i][2]));
				painter.drawString("" + (i + 1), (float) positionxDefaut, positionyDefaut);
				for (int j = 0; j < meilleurTemps[0].length; j++) {
					
					
					if(j != 2){
					painter.drawString("" + meilleurTemps[i][j], (float) positionxDefaut + 100, positionyDefaut);
					}
					else{
						painter.drawString(ms.formatTime(), (float) positionxDefaut + 100, positionyDefaut);
					}
					
					positionxDefaut += 100;
				}
				positionxDefaut = 600;
				positionyDefaut += 50;
			}
			positionyDefaut = 50;
		}
		
		
		
	}

}
