package com.helmo.cellsoflife.scenes;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public abstract class Scene {
	
	
	public abstract Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException;
	// public abstract void render(GameContainer gameContainer, Graphics graphics) throws SlickException;
	public abstract void gainingFocus(GameContainer gc) throws SlickException;
	public abstract void loosingFocus(GameContainer gc) throws SlickException;
	public abstract void draw(Graphics painter) throws SlickException;
	public void keyPressed(int key, char c) {
		
		
	}
}
