package com.helmo.cellsoflife.scenes;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;


public class PauseScene extends Scene {
	
	private Image background;

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		// TODO Auto-generated method stub
		Scene result =  new PauseScene();
		
		if(gc.getInput().isKeyPressed(Input.KEY_R)){
			result = new WelcomeScene();
		}
		else if(gc.getInput().isKeyPressed(Input.KEY_Q)){
			gc.exit();
		}
		
		return result;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		background = new Image("com/helmo/cellsoflife/ressources/pausebg.png");
		
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		painter.drawImage(background, 0, 0);
		painter.drawString("MENU DE PAUSE", 100, 30);
		painter.drawString("> (R)etourner � l'accueil du jeu", 100, 100);
		painter.drawString("> (Q)uitter le jeu", 100, 150);
		
	}

}
