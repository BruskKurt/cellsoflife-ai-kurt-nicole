package com.helmo.cellsoflife.scenes;

import java.util.HashMap;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.helmo.cellsoflife.CellsDatabase;
import com.helmo.cellsoflife.Jeu;
import com.helmo.cellsoflife.Program;
import com.helmo.cellsoflife.models.Organisme;


public class EndGameScene extends Scene {
	private Jeu jeu;
	private String titleBox, username, alertUsername;
	private int nextPositionInNomUtilisateur = 0;
	private HashMap<Integer, Character> mapChar = new HashMap<Integer, Character>();
	private Input input;
	private CellsDatabase conn = new CellsDatabase();
	private boolean inTop = false;
	
	public EndGameScene(Jeu jeu, String titleBox){
		this.jeu = jeu;
		this.titleBox = titleBox;
	}
	
	public EndGameScene(){
		this.jeu = null;
	}



	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		Scene result = new EndGameScene(this.jeu, this.titleBox);
		
		/*
		 * Lorsque l'utilisateur appuie sur une touche, nous v�rifions si son code fait partie des cl�s
		 * de notre Map.
		 *
		 *
		 *Si non : 
		 *Nous quittons le if sans rien faire
		 *
		 *Si oui:
		 *	- Nous v�rifions si la touche enter ou backspace a �t� press�e. 
		 *		Si non : 
		 *			- Nous v�rifions si son nom d'utilisateur ne d�passe pas 10 caract�res
		 *			- Nous indiquons � la position i la valeur de la cl� de la Map qui correspond avec celle appuy�e
		 *			- On incr�mente de 1 i.
		 * 
		 * 		Si oui : 
		 * 			Si c'est backspace on supprime le dernier caract�re de nom d'utilisateur (on v�rifie d'abord si il est pas empty)
		 * 			Si c'est enter on v�rifie si nom d'utilisateur > 0, si oui on envoie la requete, sinon rien. 
		 * */
		
		
		//if(this.mapChar.containsValue(gc.getInput().getKeyName(code))){
			
		//}
				
		Input in = gc.getInput();
		
		for (HashMap.Entry<Integer, Character> entry : mapChar.entrySet()) {
			if(in.isKeyPressed(entry.getKey())){
				if(this.username.length() < 10){
					this.username += entry.getValue();
					this.nextPositionInNomUtilisateur += 1;
				}
			}
		}
		
		if(gc.getInput().isKeyDown(Input.KEY_ENTER)){
			if(this.username.length() > 0){
				this.conn.saveGame(jeu, this.username);
				result = new WelcomeScene();
			}
			else{
				System.out.println("Le nom ne peut �tre ajout�!");
			}
		}
		else if(gc.getInput().isKeyPressed(Input.KEY_DELETE)){
			if(this.username.length() > 0){
				this.username = this.username.substring(0, this.username.length() - 1);
			}
			else{
				System.out.println("Lettre non retir�");
			}
		}
		
		
		if(gc.getInput().isKeyPressed(Input.KEY_TAB)){
			result = new GameScene(new Jeu(this.jeu.getFirstOrganisme()));
		}


		return result;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		
		// On v�rifie si notre score est sup�rieure � une des valeurs du top 10 dans le classement.
		int scorePartie = (int) jeu.getScore(); // Faudra voir pq on a mis score dans un float... -,-
		int tempsPartie = jeu.getTotalTime();
		int[] topScore = conn.dixMeilleuresScoreOnly();
		int[] topTemps = conn.dixMeilleuresTempsOnly();
		
		
		
		
		for (int i = 0; i < topScore.length; i++) {
			if(scorePartie >= topScore[i]){
				inTop = true;
			}
			else{
			}
		}
		
		
		for (int i = 0; i < topTemps.length; i++) {
			if(tempsPartie <= topTemps[i]){
				inTop = true;
				
			}
			else{

			}
		}
		
		
		
		
		this.alertUsername = "";
		this.username = "";
		
		mapChar.put(30, 'A');
		mapChar.put(48, 'B');
		mapChar.put(46, 'C');
		mapChar.put(32, 'D');
		mapChar.put(18, 'E');
		mapChar.put(33, 'F');
		mapChar.put(34, 'G');
		mapChar.put(35, 'H');
		mapChar.put(23, 'I');
		mapChar.put(36, 'J');
		mapChar.put(37, 'K');
		mapChar.put(38, 'L');
		mapChar.put(50, 'M');
		mapChar.put(49, 'N');
		mapChar.put(24, 'O');
		mapChar.put(25, 'P');
		mapChar.put(16, 'Q');
		mapChar.put(19, 'R');
		mapChar.put(31, 'S');
		mapChar.put(20, 'T');
		mapChar.put(22, 'U');
		mapChar.put(47, 'V');
		mapChar.put(17, 'W');
		mapChar.put(45, 'X');
		mapChar.put(21, 'Y');
		mapChar.put(44, 'Z');

	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		painter.drawImage(new Image("com/helmo/cellsoflife/ressources/pausebg.png"), 0, 0);
		painter.setColor(Color.yellow);
		painter.drawString(this.titleBox, 100, 50);
		painter.setColor(Color.white);
		painter.drawString("R�capitulatif de votre partie:", 100, 100);
		painter.drawString("==============================", 100, 120);
		painter.drawString("Temps de jeu:" + jeu.getFormatedChrono(), 100, 150);
		painter.drawString("Score total:" + jeu.getScore(), 100, 180);
		painter.drawString("Taux d'infection:" + jeu.getPercentAffected() + " %", 100, 210);
		painter.drawString("=========> Pour rejouer la partie, appuyez sur la touche TAB.", 100, 240);
		

		
		painter.drawString("Veuillez indiquer votre nom d'utilisateur", 100, 300);
		painter.drawString("(Caract�res accept�s: A - Z | Max 10 Car.)", 100, 330);
		
		painter.setColor(Color.cyan);
		painter.drawString("Nom d'utilisateur >>>>", 100, 400);
		painter.drawString(this.alertUsername, 100, 450);
		
		painter.setColor(Color.yellow);
		// Affichage du nom
		int positionXChar = 295; // position de l'affichage de la premi�re lettre du nom d'utilisateur
		for (int i = 0; i < username.length(); i++) {
			painter.drawString(""+username.charAt(i), positionXChar += 10, 400);
		}
		
		
		
		
		
		// On averti l'utilisateur qu'il a int�gr� le classement
		
		if(this.inTop){
		painter.drawImage(new Image("com/helmo/cellsoflife/ressources/vaultboy.png"), (float)(Program.WIDTH * 0.5), Program.HEIGHT - 240);
		painter.drawString("Bravo, vous �tes dans un des top 10 ! Indiquez votre nom et ensuite allez v�rifier votre prestation! :-) ", 25 , Program.HEIGHT - 270);
		}
		else{
			painter.drawString(">>>>>>>>>>> Dommage, tu n'as pas su rejoindre un des classements! :-(", 25 , Program.HEIGHT - 270);
		}
		
	}

}
