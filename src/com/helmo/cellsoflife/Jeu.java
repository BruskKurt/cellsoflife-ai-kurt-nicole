package com.helmo.cellsoflife;


import java.util.ArrayList;
import com.helmo.cellsoflife.Program;
import com.helmo.cellsoflife.models.Chronometre;
import com.helmo.cellsoflife.models.Organisme;
import com.helmo.cellsoflife.models.Tir;
import com.helmo.cellsoflife.models.Vaisseau;

public class Jeu {
	private long score;
	private Organisme organisme, firstOrganisme;
	private Chronometre tempsDeJeu;
	private Vaisseau vaisseau;
	private ArrayList<Tir> tirs;
	private int largeurCellule;
	private int hauteurCellule;
	private double width = Program.WIDTH * 0.66;
	
	
	/**
	 * Constructeur par d�faut d'un jeu
	 */
	public Jeu() {
		//this.organisme = new Organisme(5, 5);
		this.organisme = new Organisme(30,30, false);
		this.firstOrganisme = new Organisme(organisme);
		tempsDeJeu = new Chronometre();
		vaisseau = new Vaisseau();
		this.tirs = new ArrayList<>();
		this.largeurCellule = (int)(width / getOrganismeLine());
		this.hauteurCellule = (int) (Program.HEIGHT - getHauteurVaisseau()) / getOrganismeColumn();
	}
	
	
	/**
	 *  Constructeur que l'on appelle uniquement lorsque l'on veut rejouer la m�me partie
	 * @param org organisme que l'on d�sire rejouer
	 */
	public Jeu(Organisme org){
		this.organisme = org;
		this.firstOrganisme = new Organisme(org);
		tempsDeJeu = new Chronometre();
		vaisseau = new Vaisseau();
		this.tirs = new ArrayList<>();
		this.largeurCellule = (int)(width / getOrganismeLine());
		this.hauteurCellule = (int) (Program.HEIGHT - getHauteurVaisseau()) / getOrganismeColumn();
	}
	
	
	/**
	 * Constructeur que l'on appelle depuis CellsBase pour recharger Jeu
	 * @param org organisme � affecter
	 * @param tempsDeJeu temps de jeu � affecter
	 * @param tirs tirs de jeu � affecter
	 * @param score score de jeu � affecter
	 * @param vaisseauPositionX vaisseauPositionX de jeu � affecter
	 * @param vaisseauPositionY vaisseauPositionY de jeu � affecter
	 */
	public Jeu(Organisme org, int tempsDeJeu, ArrayList<Tir> tirs, int score, int vaisseauPositionX, int vaisseauPositionY){
		this.organisme = org;
		this.firstOrganisme = new Organisme(org);
		this.tempsDeJeu = new Chronometre(tempsDeJeu);
		this.score = (long)score;
		this.tirs = tirs;
		this.vaisseau = new Vaisseau(vaisseauPositionX, vaisseauPositionY);
		this.largeurCellule = (int)(width / getOrganismeLine());
		this.hauteurCellule = (int) (Program.HEIGHT - getHauteurVaisseau()) / getOrganismeColumn();
	}
	
	public Organisme getFirstOrganisme() {
		return firstOrganisme;
	}


	/**
	 * Permet de cr�er un Jeu par d�faut mais avec un organisme personnalis�e 
	 * @param nombreDeLignes nombre de ligne personnalis�e pour l'organisme
	 * @param nombreDeColonnes nombre de colonnes personnalis�e pour l'organisme
	 */
	public Jeu(int nombreDeLignes, int nombreDeColonnes){
		this.organisme = new Organisme(nombreDeLignes, nombreDeColonnes, true);
		tempsDeJeu = new Chronometre();
		vaisseau = new Vaisseau();
		this.tirs = new ArrayList<>();
		this.largeurCellule = (int)(width / getOrganismeLine());
		this.hauteurCellule = (int) (Program.HEIGHT - getHauteurVaisseau()) / getOrganismeColumn();
	}

	/**
	 * M�thode utilisant la m�thode ad�quate de Chronom�tre pour ajouter un temps au temps de jeu.
	 * @param elapsedTimeSinceLastUpdateInMillis temps � ajouter au chrono.
	 */
	public void addTime(int elapsedTimeSinceLastUpdateInMillis){
		this.tempsDeJeu.ajouterTemps(elapsedTimeSinceLastUpdateInMillis);
	}
	
	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne le nombre de lignes de l'organisme
	 * @return nombre de lignes de l'organisme
	 */
	public int getOrganismeLine(){
		return organisme.getNumberLine();
	}
	
	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne le nombre de colonnes de l'organisme
	 * @return nombre de colonnes de l'organisme
	 */
	public int getOrganismeColumn(){
		return organisme.getNumberColumn();
	}
	
	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne le temps total de la partie
	 * @return temps total de la partie (en ms)
	 */
	public int getTotalTime(){
		return this.tempsDeJeu.getTotalTimeInMillis();
	}

	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne le pourcentage d'infection de l'organisme
	 * @return pourcentage d'infection de l'organisme
	 */
	public double getPercentAffected(){
		return organisme.getPercentAffected();
	}
	
	/**
	 * Permet de d�placer le vaisseau � droite ou � gauche avec la vitesse par d�faut du vaisseau
	 * @param string "left" pour aller � gauche, "right" pour aller � droite.
	 */
	public void deplacerVaisseau(String string) {
		/* 
		 * On doit v�rifier avant que la position du vaisseau est entre 0 et les 1/3 de la fen�tre de disponible.
		 * 
		 * */
		if(string.equals("left")){
				if(vaisseau.getPositionX() > 0){
				this.vaisseau.seDeplacer(-vaisseau.getVitesse());
				}
		}
		else if(string.equals("right")){
			
			
			if(vaisseau.getPositionX() < ((Program.WIDTH  * 0.66) - (vaisseau.getLargeur() * 0.5))){
			this.vaisseau.seDeplacer();
			}
		}
		else{
			throw new IllegalArgumentException("veuillez entrer uniquement 'left' ou 'right'.");
		}
	}
	
	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne la position en x du vaisseau 
	 * @return pourcentage d'infection de l'organisme
	 */
	public int positionVaisseau(){
		return vaisseau.getPositionX();
	}

	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne l'�tat d'une cellule
	 * @param n ligne de la cellule
	 * @param m colonne de la cellule
	 * @return �tat de la celulle (true= infect�, false= saine)
	 */
	public boolean getStatusCell(int n, int m) {
		return organisme.getCell(n,m);
	}
	
	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne le statut de l'organisme
	 */
	public void getStatusOrganisme(){
		organisme.getOrganisme();
	}

	/**
	 * M�thode utilisant la m�thode ad�quate qui retourne la hauteur du vaisseau
	 * @return la hautuer du vaisseau
	 */
	public int getHauteurVaisseau() {
		return vaisseau.getHauteur();
	}

	/**
	 * M�thode utilisant la m�thode ad�quate qui permet de d�finir un �tat d'une cellule
	 * @param i ligne de la cellule
	 * @param j colonne de la cellule
	 * @param state �tat � affect� � la cellule
	 */
	public void setCells(int i, int j, boolean state) {
		organisme.setCells(i, j, state);
	}
	
	
	/**
	 * M�thode permettant de tirer un m�dicament
	 * @param elapsedTimeSinceLastUpdateInMillis tempsTotal du jeu � affecter au tir
	 */
	public void tirerMedicament(int elapsedTimeSinceLastUpdateInMillis) {

		if(tirs.isEmpty()){
			tirs.add(new Tir(vaisseau.getPositionX(), Program.HEIGHT - vaisseau.getHauteur() - 10, tempsDeJeu.getTotalTimeInMillis()));
		}
		else if(peutTirer() == true){
			tirs.add(new Tir(vaisseau.getPositionX(), Program.HEIGHT - vaisseau.getHauteur() - 10, tempsDeJeu.getTotalTimeInMillis()));
		}		
		else{
		}
	}
	
	/**
	 * M�thode permettant de savoir si le temps n�cessaire avant de pouvoir tirer � nouveau a bien �t� pass�e
	 * @return true si l'on peut re-tirer, false si le temps n�cessaire pour tirer � nouveau n'a pas �t� atteint.
	 */
	public boolean peutTirer(){
		boolean peutTirer = false;
		if(tempsDeJeu.getTotalTimeInMillis() - (tirs.get(tirs.size()-1).getmsShoted()) >= 100 ){
			peutTirer = true;
		}	
		return peutTirer;
	}

	/**
	 * M�thode qui retourne l'instance du vaisseau de la partie
	 * @return objet vaisseau
	 */
	public Vaisseau getVaisseau() {
		return vaisseau;
	}

	/**
	 * M�thode qui permet d'affecter un nouveau Vaisseau � la partie
	 * @return objet vaisseau
	 */
	public void setVaisseau(Vaisseau vaisseau) {
		this.vaisseau = vaisseau;
	}

	/**
	 * Permet d'affecter un nouveau ArrayList de tirs
	 * @param tirs liste de tirs
	 */
	public void setTirs(ArrayList<Tir> tirs) {
		this.tirs = tirs;
	}

	/**
	 * Retourne la liste des tirs de la partie en cours
	 * @return liste de tirs
	 */
	public ArrayList<Tir> getTirs() {
		return tirs;
	}


	/**
	 * M�thode g�rant le d�placement des tirs et des supressions des tirs
	 *  de la liste lorsque ceux-ci quittent la fen�tre du jeu 
	 */
	public void moveShots() {
		if(tirs.isEmpty()){
		}
		else{
			for(int i = 0; i < tirs.size() ; i++){
				if(tirs.get(i).getPositionY() == 0){
					tirs.remove(i);
				}
				else{
					tirs.get(i).seDeplacer();
				}
			}
		}
	}
	
	
	
	
	/**
	 * M�thode permettant de d�tecter la collision des tirs avec 
	 * les cellules infect�s et d'effectuer les op�rations n�cessaires pour maintenir la coh�rence du jeu
	 */
	public void detectHit(){
		
		for (int i = 0; i < tirs.size(); i++) {
			int positionI = tirs.get(i).getPositionX() / largeurCellule;
			int positionJ = tirs.get(i).getPositionY() / hauteurCellule - 1;

			
			
			if(positionI <= organisme.getNumberLine() && positionJ >= 0){
				if(organisme.getCell(positionI, positionJ) == true){
					this.score += 100;
					this.organisme.setCells(positionI, positionJ, false); 
				}
			}

		}
	}

	/**
	 * Retourne le temps de jeu total dans un format format�e (HH:MM:SS)
	 * @return un String au format HH:MM:SS du temps total de la partie
	 */
	public String getFormatedChrono() {
		return this.tempsDeJeu.formatTime();
	}
	
	/**
	 * Retourne l'organisme de la partie actuelle
	 * @return
	 */
	public Organisme getOrganisme(){
		return this.organisme;
	}

	/**
	 * Permet d'appliquer le jeu de conway � la partie
	 * @param elapsedTimeInMillis temps de jeu total depuis la derni�re update
	 */
	public void jeuDeConway(int elapsedTimeInMillis){
		organisme.changerEtatEnFonctionDuJeuDeConway(elapsedTimeInMillis);
	}
	
	/**
	 * Retourne le score total
	 * @return score total (float)
	 */
	public float getScore() {
		return this.score;
	}

	/**
	 * Retourne la position en Y du vaisseau
	 * @return
	 */
	public float positionVaisseauY() {
		return this.vaisseau.getPositionY();
	}

	
	
}
