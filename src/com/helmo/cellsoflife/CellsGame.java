package com.helmo.cellsoflife;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import com.helmo.cellsoflife.scenes.Scene;
import com.helmo.cellsoflife.scenes.WelcomeScene;

import com.helmo.cellsoflife.scenes.Scene;
import com.helmo.cellsoflife.scenes.WelcomeScene;

public class CellsGame extends BasicGame {
	private Scene currentScene = null;
	private Scene nextScene;

	public CellsGame(String title) throws SlickException {
		super("HELMo - Cell Games");
	}
	public CellsGame(Scene sc){
		super("dunno bro");
		currentScene = sc;
	}
	// Constructeur alternatif ne demandant pas d'indiquer un titre pour la fen�tre..
	public CellsGame(){
		super("HELMo - Cell Games");
	}

	@Override
	public void render(GameContainer arg0, Graphics arg1) throws SlickException {
		currentScene.draw(arg1);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		currentScene = new WelcomeScene();
		nextScene = currentScene; // par d�faut, nous consid�rons que la prochaine scene est la m�me que l'actuelle.
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		nextScene = currentScene.update(arg0, arg1);

		/* On v�rifie s'il y a eu un changement de scene.
		 * Pour cela, on doit comparer currentScene avec nextScene.
		 * un d�gueu getClass().getSimpleName() des familles pour voir si c'est �gal;
		 * Si �a ne l'est pas, on loose/gain focus et on switch currentScene en nextScene et 
		 * nextScene = currentScene (gard� l'�galit� des getSimpleName et valider la condition if)
		 * */
		if(currentScene.getClass().getSimpleName().equals(nextScene.getClass().getSimpleName())){
			// Pas de changement de scene;
		}
		else{

			currentScene.loosingFocus(arg0);
			nextScene.gainingFocus(arg0);
			currentScene = nextScene;
			nextScene = currentScene;
		}
		
		
		
	}

}
