package tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.helmo.cellsoflife.models.Organisme;


public class TestsOrganisme {
	
	@Test
	public void VoisinsCelluleSansDebordements() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true); 
		org.setCells(0, 1, true);
		org.setCells(0, 2, true);
		org.setCells(2, 0, true);
		org.setCells(2, 1, true);
		org.setCells(2, 2, true);
		org.setCells(1, 0, true);
		org.setCells(1, 2, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(1, 1));
	}
	
	@Test
	public void VoisinsDebordementAuNord() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(0, 1, true);
		org.setCells(1, 1, true);
		org.setCells(2, 0, true);
		org.setCells(2, 1, true);
		org.setCells(1, 29, true);
		org.setCells(0, 29, true);
		org.setCells(2, 29, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(1, 0));
	}
	
	@Test
	public void VoisinsDebordementAuSud() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(1, 0, true);
		org.setCells(2, 0, true);
		org.setCells(0, 29, true);
		org.setCells(2, 29, true);
		org.setCells(1, 28, true);
		org.setCells(0, 28, true);
		org.setCells(2, 28, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(1, 29));
	}
	
	@Test
	public void VoisinsDebordementEst() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(29, 0, true);
		org.setCells(29, 2, true);
		org.setCells(28, 0, true);
		org.setCells(28, 1, true);
		org.setCells(28, 2, true);
		org.setCells(0, 1, true);
		org.setCells(0, 0, true);
		org.setCells(0, 2, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(29, 1));
	}
	
	@Test
	public void VoisinsDebordementOuest() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(0, 2, true);
		org.setCells(1, 0, true);
		org.setCells(1, 1, true);
		org.setCells(1, 2, true);
		org.setCells(29, 0, true);
		org.setCells(29, 1, true);
		org.setCells(29, 2, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(0, 1));
	}
	
	@Test
	public void VoisinsDebordementAuNordOuest() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(29, 29, true);
		org.setCells(1, 0, true);
		org.setCells(0, 1, true);
		org.setCells(1, 1, true);
		org.setCells(0, 29, true);
		org.setCells(1, 29, true);
		org.setCells(29, 1, true);
		org.setCells(29, 0, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(0, 0));
	}
	
	@Test
	public void VoisinsDebordementAuNordEst() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(0, 1, true);
		org.setCells(0, 29, true);
		org.setCells(29, 29, true);
		org.setCells(28, 29, true);
		org.setCells(29, 1, true);
		org.setCells(28, 0, true);
		org.setCells(28, 1, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(29, 0));
	}
	
	@Test
	public void VoisinsDebordementAuSudOuest() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(1, 0, true);
		org.setCells(29, 29, true);
		org.setCells(29, 28, true);
		org.setCells(0, 28, true);
		org.setCells(1, 29, true);
		org.setCells(1, 28, true);
		org.setCells(29, 0, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(0, 29));
	}
	
	@Test
	public void VoisinsDebordementAuSudEst() {
		Organisme org = new Organisme(30, 30, false);
				
		org.setCells(0, 0, true);
		org.setCells(29, 0, true);
		org.setCells(28, 0, true);
		org.setCells(0, 29, true);
		org.setCells(0, 28, true);
		org.setCells(28, 29, true);
		org.setCells(29, 28, true);
		org.setCells(28, 28, true);

		
		assertEquals(8, org.calculeNbVoisinsContamines(29, 29));
	}
	
	

}
