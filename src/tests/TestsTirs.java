package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import com.helmo.cellsoflife.Jeu;



public class TestsTirs {
	
	@Test
	public void premierTirSeRajoute() {
		Jeu jeu = new Jeu();
		jeu.tirerMedicament(0);
		assertEquals(1, jeu.getTirs().size());
	}
	
	@Test
	public void tirsTropRapideNeSeRajoutePas() {
		Jeu jeu = new Jeu();
		jeu.tirerMedicament(0); // Premier tir � 0ms
		jeu.addTime(90); // On ne rajoute que 90ms au temps de jeu
		jeu.tirerMedicament(5); // On tire 5ms plus tard apr�s l'ajout du temps (donc 95ms, pas assez de d�lai)
		assertEquals(1, jeu.getTirs().size()); // Le deuxieme tir n'est pas pris en compte.
	}
	
	@Test
	public void tirSeRajouteApres100ms() {
		Jeu jeu = new Jeu();
		jeu.tirerMedicament(0); // Premier tir � 0ms
		jeu.addTime(100); // On ajoute 100ms au temps de jeu
		jeu.tirerMedicament(0); // On tire le deuxi�me tir avec 0ms en plus du temps de jeu �coul�
		assertEquals(2, jeu.getTirs().size());
	}
	
	@Test
	public void lesTirsSeSupprimentQuandIlsQuittentLaZoneDeJeu() {
		Jeu jeu = new Jeu();
		jeu.tirerMedicament(100); // Premier tir � 100ms
		jeu.addTime(100); // On ajoute 100ms au temps de jeu
		jeu.tirerMedicament(1); // On tire le deuxi�me tir avec 0ms en plus du temps de jeu �coul�
		
		/*Notre m�thode moveShots s'adapte � la taille de la fen�tre du jeu: nous utilsons donc Program.WIDTH.
		 * C'est pourquoi nous allons "tricher" et boucler beaucoup de fois pour 
		 * imiter le d�placement sur notre fen�tre principale (qui peut �tre 600px comme 1080px...)
		 * */
		
		// Notre fenetre fait 768 - hauteur du vaisseau - une petite marge de bordure : 710px.
		for (int i = 0; i < 710 ; i++) {
			jeu.moveShots();
		}

		assertEquals(0, jeu.getTirs().size());
	}
	
	@Test
	public void unTirSeSupprimeQuandIlQuitteLaZone(){

	}

}
